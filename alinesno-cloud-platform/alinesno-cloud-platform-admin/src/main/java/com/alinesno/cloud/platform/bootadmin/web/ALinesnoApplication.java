package com.alinesno.cloud.platform.bootadmin.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * SpringBoot2 Admin监控中心
 * 
 * @author LuoAnDong
 * @since 2019年2月16日 上午8:46:14
 */
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class ALinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ALinesnoApplication.class, args);
	}

	@Configuration
    public static class SecurityPermitAllConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests().anyRequest().permitAll()  
                .and().csrf().disable();
        }
    }
	
}