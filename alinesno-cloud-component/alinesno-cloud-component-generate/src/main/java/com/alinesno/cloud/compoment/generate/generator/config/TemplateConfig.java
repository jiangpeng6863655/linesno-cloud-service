/**
 * Copyright (c) 2011-2020, hubin (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.alinesno.cloud.compoment.generate.generator.config;

/**
 * 模板路径配置项
 * @author LuoAnDong
 * @since 2018年12月2日 上午7:31:07
 */
public class TemplateConfig {

    private String entity = ConstVal.TEMPLATE_ENTITY;

    private String service = ConstVal.TEMPLATE_SERVICE;

    private String serviceImpl = ConstVal.TEMPLATE_SERVICEIMPL;

    private String mapper = ConstVal.TEMPLATE_MAPPER;
   
    // 针对于Spring Cloud Data JPA 实现
    private String repository = ConstVal.TEMPLATE_REPOSITORY;
    
    // 针对于Spirng Cloud Rest接口实现 
    private String restController = ConstVal.TEMPLATE_REST_CONTROLLER ;
    
    // 针对于Spring Cloud Feigin接口实现
    private String feigin = ConstVal.TEMPLATE_FEIGN_API ;
    private String feiginDto = ConstVal.TEMPLATE_FEIGN_DTO ;

//    private String xml = ConstVal.TEMPLATE_XML;
    private String controller = ConstVal.TEMPLATE_CONTROLLER;
   
    // 针对于 springboot 启动类
    private String boot = ConstVal.TEMPLATE_BOOT ; 
    
    private String pageList = ConstVal.TEMPLATE_JSP_LIST_PATH;
    private String pageAdd = ConstVal.TEMPLATE_JSP_ADD_PATH;
    private String pageModify = ConstVal.TEMPLATE_JSP_MODIFY_PATH;
    private String pageDetail = ConstVal.TEMPLATE_JSP_DETAIL_PATH;

    public String getPageDetail() {
		return pageDetail;
	}

	public void setPageDetail(String pageDetail) {
		this.pageDetail = pageDetail;
	}

	public String getEntity() {
        return entity;
    }

    public TemplateConfig setEntity(String entity) {
        this.entity = entity;
        return this;
    }

    public String getService() {
        return service;
    }

    public TemplateConfig setService(String service) {
        this.service = service;
        return this;
    }

    public String getRepository() {
		return repository;
	}

	public void setRepository(String repository) {
		this.repository = repository;
	}

	public String getServiceImpl() {
        return serviceImpl;
    }

    public TemplateConfig setServiceImpl(String serviceImpl) {
        this.serviceImpl = serviceImpl;
        return this;
    }

    public String getMapper() {
        return mapper;
    }

    public TemplateConfig setMapper(String mapper) {
        this.mapper = mapper;
        return this;
    }

//    public String getXml() {
//        return xml;
//    }
//
//    public TemplateConfig setXml(String xml) {
//        this.xml = xml;
//        return this;
//    }

    public String getController() {
        return controller;
    }

    public TemplateConfig setController(String controller) {
        this.controller = controller;
        return this;
    }

	public String getPageList() {
		return pageList ; 
	}

	public String getPageAdd() {
		return pageAdd;
	}

	public void setPageAdd(String pageAdd) {
		this.pageAdd = pageAdd;
	}

	public String getPageModify() {
		return pageModify;
	}

	public void setPageModify(String pageModify) {
		this.pageModify = pageModify;
	}

	public void setPageList(String pageList) {
		this.pageList = pageList;
	}

	public String getRestController() {
		return restController;
	}

	public void setRestController(String restController) {
		this.restController = restController;
	}

	public String getFeigin() {
		return feigin;
	}

	public void setFeigin(String feigin) {
		this.feigin = feigin;
	}

	public String getFeiginDto() {
		return feiginDto;
	}

	public void setFeiginDto(String feiginDto) {
		this.feiginDto = feiginDto;
	}

	public String getBoot() {
		return boot;
	}

	public void setBoot(String boot) {
		this.boot = boot;
	}

}
