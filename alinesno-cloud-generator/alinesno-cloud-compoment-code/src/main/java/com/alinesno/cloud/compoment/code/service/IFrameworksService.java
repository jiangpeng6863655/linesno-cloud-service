package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.FrameworksEntity;
import com.alinesno.cloud.compoment.code.repository.FrameworksRepository;

/**
 * <p> 框架技术池 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IFrameworksService extends IBaseService<FrameworksRepository, FrameworksEntity, String> {

}
