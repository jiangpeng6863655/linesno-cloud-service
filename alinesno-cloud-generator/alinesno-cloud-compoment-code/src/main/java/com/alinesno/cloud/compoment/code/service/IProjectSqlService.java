package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.ProjectSqlEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectSqlRepository;

/**
 * <p> 项目sql脚本 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IProjectSqlService extends IBaseService<ProjectSqlRepository, ProjectSqlEntity, String> {

}
