package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.MapFieldColumnEntity;
import com.alinesno.cloud.compoment.code.repository.MapFieldColumnRepository;
import com.alinesno.cloud.compoment.code.service.IMapFieldColumnService;

/**
 * <p> 字段属性映射信息 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class MapFieldColumnServiceImpl extends IBaseServiceImpl<MapFieldColumnRepository, MapFieldColumnEntity, String> implements IMapFieldColumnService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(MapFieldColumnServiceImpl.class);

}
