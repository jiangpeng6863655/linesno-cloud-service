package com.alinesno.cloud.common.facade.feign;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.common.facade.wrapper.Wrapper;

/**
 * 基础调用接口
 * 
 * @author LuoAnDong
 * @since 2018年12月2日 下午1:11:26
 * @param <DTO>
 */
public interface IBaseFeign<E> {

	@GetMapping("findAll")
	public List<E> findAll();

	@PostMapping("findAllById")
	public List<E> findAllById(@RequestBody Iterable<String> ids);

	@PostMapping(value="findAllByPageable" , consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Page<E> findAll(@RequestParam("page") Pageable page);

	@PostMapping(value="findAllByWrapper" ,  consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<E> findAll(@RequestBody Wrapper restWrapper);

	@PostMapping(value="findOneByWrapper" ,  consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Optional<E> findOne(@RequestBody Wrapper restWrapper);

	@PostMapping(value="findAllByWrapperAndPageable" ,  consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public Page<E> findAllByWrapperAndPageable(@RequestBody Wrapper restWrapper  ,@RequestParam("page") Pageable page);
	public Page<E> findAllByWrapperAndPageable(@RequestBody Wrapper restWrapper) ; 

	@PostMapping("saveAll")
	public List<E> saveAll(@RequestBody Iterable<E> entities);

	@PostMapping("saveAndFlush")
	public E saveAndFlush(@RequestBody E entity);

	@PostMapping("deleteInBatch")
	public void deleteInBatch(@RequestBody Iterable<E> entities);
	
	@PostMapping("deleteByIds")
	public void deleteByIds(@RequestBody String[] ids);

	@GetMapping("deleteAllInBatch")
	public void deleteAllInBatch();

	@GetMapping("getOne")
	public E getOne(@RequestParam("id") String id);

	@PostMapping("save")
	public E save(@RequestBody E entity);

	@GetMapping("findById")
	public Optional<E> findById(@RequestParam("id") String id);

	@GetMapping("existsById")
	public boolean existsById(@RequestParam("id") String id);

	@GetMapping("count")
	public long count();

	@GetMapping("deleteById")
	public void deleteById(@RequestParam("id") String id);

	@PostMapping("delete")
	public void delete(@RequestBody E entity);

	@PostMapping("deleteAllByIterable")
	public void deleteAll(@RequestBody Iterable<E> entities);

	@PostMapping("deleteAll")
	public void deleteAll();

	/**
	 * 更新实体状态 
	 * @param id
	 * @return
	 */
	@GetMapping("modifyHasStatus")
	boolean modifyHasStatus(@RequestParam("id") String id);

	@GetMapping("findAllByApplicationId")
	List<E> findAllByApplicationId(@RequestParam("applicationId") String applicationId);
	
	@GetMapping("findAllByTenantId")
	List<E> findAllByTenantId(@RequestParam("tenantId") String tenantId);
}
