package com.alinesno.cloud.common.web.enable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.common.core.auto.CoreImportProvider;
import com.alinesno.cloud.common.core.config.AsyncConfig;
import com.alinesno.cloud.common.web.base.advice.DatagridResponseBodyAdvice;
import com.alinesno.cloud.common.web.base.advice.plugins.ApplicationPlugin;
import com.alinesno.cloud.common.web.base.advice.plugins.DirectoryPlugin;
import com.alinesno.cloud.common.web.base.controller.PageJumpController;
import com.alinesno.cloud.common.web.base.controller.WebStorageController;
import com.alinesno.cloud.common.web.base.exceptions.GlobalExceptionHandler;
import com.alinesno.cloud.common.web.base.feign.FeignConfig;
import com.alinesno.cloud.common.web.base.form.FormTokenInterceptor;
import com.alinesno.cloud.common.web.base.thymeleaf.TagsDialect;
import com.alinesno.cloud.common.web.base.utils.WebUploadUtils;
import com.alinesno.cloud.common.web.login.aop.AccountRecordAspect;
import com.alinesno.cloud.common.web.login.controller.DashboardController;
import com.alinesno.cloud.common.web.login.controller.LoginController;
import com.alinesno.cloud.common.web.login.session.RedisSessionConfig;
import com.alinesno.cloud.common.web.login.shiro.AccountRealm;
import com.alinesno.cloud.common.web.login.shiro.cofnig.ShiroConfiguration;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class CommonLoginConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>() ; 

		// common core 
		List<String> coreLoader = CoreImportProvider.classLoader() ; 
		importBean.addAll(coreLoader) ; 
	
		// common web 
		importBean.add(FormTokenInterceptor.class.getName()) ; 
		importBean.add(GlobalExceptionHandler.class.getName()) ; 
		importBean.add(WebUploadUtils.class.getName()) ; 
		importBean.add(TagsDialect.class.getName()) ; 
		
		importBean.add(WebStorageController.class.getName()) ; 
		importBean.add(PageJumpController.class.getName()) ; 
		importBean.add(DatagridResponseBodyAdvice.class.getName()) ; 
		importBean.add(DirectoryPlugin.class.getName()) ; 
		importBean.add(ApplicationPlugin.class.getName()) ; 
		
		importBean.add(FeignConfig.class.getName()) ; 
		importBean.add(AsyncConfig.class.getName()) ;  // 添加异步线程池
	
		// common web 
		importBean.add(AccountRecordAspect.class.getName()) ; 
		
		// common login  
		importBean.add(ShiroConfiguration.class.getName()) ; 
		importBean.add(AccountRealm.class.getName()) ; 
		importBean.add(RedisSessionConfig.class.getName()) ; 
		importBean.add(LoginController.class.getName()) ; 
		importBean.add(DashboardController.class.getName()) ; 
				
		return importBean.toArray(new String[] {}) ;
	}

	
}
