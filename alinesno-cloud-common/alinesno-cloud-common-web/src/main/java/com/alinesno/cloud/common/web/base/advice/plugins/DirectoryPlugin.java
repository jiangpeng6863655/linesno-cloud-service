package com.alinesno.cloud.common.web.base.advice.plugins;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.feign.dto.ManagerCodeDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerCodeFeigin;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 代码转换插件
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:23:50
 */
@Component
public class DirectoryPlugin implements TranslatePlugin {

	@Autowired
	private ManagerCodeFeigin managerCodeFeigin ; 
	
	private static Map<String , ManagerCodeDto> listCode = null ; 
	
	@Override
	public void translate(ObjectNode node, TranslateCode convertCode) {

		// 解析注解
		String fieldJson = convertCode.value() ; 
		if(StringUtils.isBlank(fieldJson)) {
			return ; 
		}
		
		JsonArray fieldArr = (JsonArray)parser.parse(fieldJson);
					
		log.debug("node = {}" , node); 
		for(JsonElement convertEle : fieldArr) {
			
			JsonObject convertObj = convertEle.getAsJsonObject() ;
			for (String convertFieldName :convertObj.keySet()) {
				
				String convertFieldValue = node.get(convertFieldName).asText() ; // node.getAsJsonObject().get(convertFieldName).getAsString() ; 
				String convertFieldCodeType = convertObj.get(convertFieldName).getAsString() ; 
				
				log.debug("fieldName = {} , codeType = {} , fieldValue = {}" , convertFieldName , convertFieldCodeType , convertFieldValue);
				
				node.put(convertFieldName+"Label", queryName(convertFieldCodeType , convertFieldValue)); 
			}
		}	
	}

	/**
	 * 初始化代码字典
	 * @param managerCodeFeigin2
	 * @return 
	 */
	private String queryName(String codeType , String codeValue) {
		if(listCode == null) { // 第一次初始化
			
			listCode = new HashMap<String , ManagerCodeDto>() ; 
			
			List<ManagerCodeDto> list = managerCodeFeigin.findAll() ; 
			for(ManagerCodeDto l : list) {
				listCode.put(l.getCodeTypeValue()+"_"+l.getCodeValue() , l) ;
			}
			
		}
		
		// TODO 待优化
		ManagerCodeDto m = listCode.get(codeType+"_"+codeValue) ; 
		return m == null?null:m.getCodeName(); 
	}

}
