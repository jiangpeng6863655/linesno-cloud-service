package com.alinesno.cloud.common.core.auto;

import java.util.ArrayList;
import java.util.List;

import com.alinesno.cloud.common.core.context.ApplicationContextProvider;

/**
 * 对外提供核心对象,便于springboot实现enable功能
 * @author LuoAnDong
 * @since 2019年4月7日 下午2:41:44
 */
public class CoreImportProvider {

	/**
	 * 提供接口对象 
	 * @return
	 */
	public static List<String> classLoader(){
		List<String> s = new ArrayList<>() ; 
		
		s.add(ApplicationContextProvider.class.getName()) ;  
		
		return s ; 
	}
	
}
