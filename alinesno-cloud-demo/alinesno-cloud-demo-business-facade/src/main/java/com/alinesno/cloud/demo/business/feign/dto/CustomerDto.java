package com.alinesno.cloud.demo.business.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * 日志记录
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:37:44
 */
@SuppressWarnings("serial")
public class CustomerDto extends BaseDto {

	private String customerNo; // 编号
	private String customerName; // 客户名称
	private String customerType ; // 客户类别
	private String consumerAddress ;// 客户地址
	private String connectPhone ; // 联系电话
	private String connectMan ; // 联系人
	
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getConsumerAddress() {
		return consumerAddress;
	}
	public void setConsumerAddress(String consumerAddress) {
		this.consumerAddress = consumerAddress;
	}
	public String getConnectPhone() {
		return connectPhone;
	}
	public void setConnectPhone(String connectPhone) {
		this.connectPhone = connectPhone;
	}
	public String getConnectMan() {
		return connectMan;
	}
	public void setConnectMan(String connectMan) {
		this.connectMan = connectMan;
	}

}
