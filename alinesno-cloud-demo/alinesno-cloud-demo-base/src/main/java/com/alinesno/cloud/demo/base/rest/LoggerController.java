package com.alinesno.cloud.demo.base.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.common.core.rest.BaseRestController;
import com.alinesno.cloud.demo.base.entity.LoggerEntity;
import com.alinesno.cloud.demo.base.services.ILoggerService;

/**
 * 日志控制restful服务，对外提供接口
 * @author LuoAnDong
 * @since 2018年12月11日 上午7:39:12
 */
@RestController
@RequestMapping("logger")
public class LoggerController extends BaseRestController<LoggerEntity , ILoggerService> {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(LoggerController.class);
	
}
