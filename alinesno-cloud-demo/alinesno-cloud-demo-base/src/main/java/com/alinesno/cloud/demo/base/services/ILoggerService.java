package com.alinesno.cloud.demo.base.services;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.demo.base.entity.LoggerEntity;
import com.alinesno.cloud.demo.base.repository.LoggerRepository;

@NoRepositoryBean
public interface ILoggerService extends IBaseService<LoggerRepository, LoggerEntity, String> {

}
