package com.alinesno.cloud.demo.base.repository;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.demo.base.entity.LoggerEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface LoggerRepository extends IBaseJpaRepository<LoggerEntity, String> {

}