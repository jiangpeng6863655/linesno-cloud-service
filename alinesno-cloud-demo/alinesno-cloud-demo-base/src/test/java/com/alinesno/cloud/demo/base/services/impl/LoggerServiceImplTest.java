package com.alinesno.cloud.demo.base.services.impl;

import static org.junit.Assert.fail;

import java.util.UUID;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.alinesno.cloud.common.core.junit.JUnitBase;
import com.alinesno.cloud.common.core.orm.id.SnowflakeIdWorker;
import com.alinesno.cloud.demo.base.entity.LoggerEntity;
import com.alinesno.cloud.demo.base.services.ILoggerService;

public class LoggerServiceImplTest extends JUnitBase {

	@Autowired
	private ILoggerService loggerService ; 
	
	@Test
	public void testFindAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllById() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPageable() {
		Pageable page = PageRequest.of(1, 5) ; 
		
		Page<LoggerEntity> loggerPage = loggerService.findAll(page) ; 
		
		log.debug("logger page = {}" , ToStringBuilder.reflectionToString(loggerPage));
	}

	@Test
	public void testSave() throws InterruptedException {
		LoggerEntity e = new LoggerEntity() ; 
		
		e.setRecordChannel("日志通道");
		e.setRecordMsg("日志信息:"+System.currentTimeMillis());
		e.setRecordParams("uuid="+UUID.randomUUID());
		e.setRecordType("type:"+SnowflakeIdWorker.getId());
		e.setRecordUser("123456");
		e.setRecordUserName("张三");
		
		e = loggerService.save(e); 
	
		Thread.sleep(5000);
	
		e.setRecordUser("李四");
		e = loggerService.save(e); 
		
		log.debug("e = {}" , e);
		
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testExistsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllIterableOfQextendsEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testExists() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntityPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntitySort() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountSpecificationOfEntity() {
		fail("Not yet implemented");
	}

}
