package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerApplicationDto extends BaseDto {

    /**
     * 应用名称
     */
	private String applicationName ;
	
    /**
     * 应用描述
     */
	private String applicationDesc;
	
    /**
     * 应用图标 
     */
	private String applicationIcons;

	/**
	 * 应用链接
	 */
	private String applicationLink ; 
	
    /**
     * 父类id
     */
	private String pid;
	
	
	public String getApplicationLink() {
		return applicationLink;
	}

	public void setApplicationLink(String applicationLink) {
		this.applicationLink = applicationLink;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationDesc() {
		return applicationDesc;
	}

	public void setApplicationDesc(String applicationDesc) {
		this.applicationDesc = applicationDesc;
	}

	public String getApplicationIcons() {
		return applicationIcons;
	}

	public void setApplicationIcons(String applicationIcons) {
		this.applicationIcons = applicationIcons;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

}
