package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class UserAddressDto extends BaseDto {

    /**
     * 所属者
     */
	private String owners;
	
    /**
     * 地址名称
     */
	private String addressName;
	
    /**
     * 是否使用
     */
	private String isUse;
	
    /**
     * 结束使用时间
     */
	private String useEndTime;
	
    /**
     * 开始使用时间
     */
	private String useStartTime;
	
    /**
     * 所属用户
     */
	private String userId;
	
    /**
     * 楼层
     */
	private String floor;
	
    /**
     * 房间号
     */
	private String roomNum;
	


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getAddressName() {
		return addressName;
	}

	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public String getUseEndTime() {
		return useEndTime;
	}

	public void setUseEndTime(String useEndTime) {
		this.useEndTime = useEndTime;
	}

	public String getUseStartTime() {
		return useStartTime;
	}

	public void setUseStartTime(String useStartTime) {
		this.useStartTime = useStartTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}

}
