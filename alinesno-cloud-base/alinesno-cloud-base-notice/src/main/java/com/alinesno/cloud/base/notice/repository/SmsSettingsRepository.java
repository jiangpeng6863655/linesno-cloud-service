package com.alinesno.cloud.base.notice.repository;

import com.alinesno.cloud.base.notice.entity.SmsSettingsEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 06:27:10
 */
public interface SmsSettingsRepository extends IBaseJpaRepository<SmsSettingsEntity, String> {

}
