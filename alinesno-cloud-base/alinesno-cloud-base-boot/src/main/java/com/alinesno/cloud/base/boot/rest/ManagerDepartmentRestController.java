package com.alinesno.cloud.base.boot.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.base.boot.service.IManagerDepartmentService;
import com.alinesno.cloud.common.core.rest.BaseRestController;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2019-03-24 13:24:58
 */
@Scope("prototype")
@RestController
@RequestMapping("managerDepartment")
public class ManagerDepartmentRestController extends BaseRestController<ManagerDepartmentEntity , IManagerDepartmentService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(ManagerDepartmentRestController.class);

	@PostMapping("findAllWithApplication")
	List<ManagerDepartmentEntity> findAllWithApplication(@RequestBody RestWrapper restWrapper){
		return feign.findAllWithApplication(restWrapper) ; 
	}

}
