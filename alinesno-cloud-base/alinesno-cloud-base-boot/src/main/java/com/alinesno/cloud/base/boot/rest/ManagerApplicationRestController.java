package com.alinesno.cloud.base.boot.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Scope("prototype")
@RestController
@RequestMapping("managerApplication")
public class ManagerApplicationRestController extends BaseRestController<ManagerApplicationEntity , IManagerApplicationService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(ManagerApplicationRestController.class);

	/**
	 * 根据用户查询用户的所属应用
	 * @param id
	 * @return
	 */
	@GetMapping("findAllByAccountId")
	List<ManagerApplicationEntity> findAllByAccountId(String accountId){
		return feign.findAllByAccountId(accountId) ; 
	}
}
