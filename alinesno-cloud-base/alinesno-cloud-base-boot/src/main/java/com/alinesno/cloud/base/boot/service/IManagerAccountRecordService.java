package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRecordEntity;
import com.alinesno.cloud.base.boot.repository.ManagerAccountRecordRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 22:18:49
 */
@NoRepositoryBean
public interface IManagerAccountRecordService extends IBaseService<ManagerAccountRecordRepository, ManagerAccountRecordEntity, String> {

}
