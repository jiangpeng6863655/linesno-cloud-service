package com.alinesno.cloud.base.boot.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.ContentPostTypeEntity;
import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.enums.ResourceTypeEnmus;
import com.alinesno.cloud.base.boot.repository.ContentPostTypeRepository;
import com.alinesno.cloud.base.boot.service.IContentPostTypeService;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class ContentPostTypeServiceImpl extends IBaseServiceImpl<ContentPostTypeRepository, ContentPostTypeEntity, String> implements IContentPostTypeService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ContentPostTypeServiceImpl.class);
	
	@Autowired
	private IManagerApplicationService managerApplicationService ; 
	
	@Override
	public List<ContentPostTypeEntity> findAllWithApplication(RestWrapper restWrapper) {
		List<ContentPostTypeEntity> list = jpa.findAll() ; 
		
		List<ManagerApplicationEntity> apps = managerApplicationService.findAll() ; 
		
		for(ManagerApplicationEntity app : apps) {
			ContentPostTypeEntity d = new ContentPostTypeEntity() ; 
			d.setPid(ResourceTypeEnmus.PLATFORM_RESOURCE_PARENT.value);
			d.setId(app.getId());
			d.setTypeName(app.getApplicationName());
			
			for(ContentPostTypeEntity b : list) {
				if(app.getId().equals(b.getApplicationId()) && b.getPid().equals(ResourceTypeEnmus.PLATFORM_RESOURCE_PARENT.value)) {
					b.setPid(app.getId());
				}
			}
			list.add(d) ; 
		}
		
		return list;
	}

}
