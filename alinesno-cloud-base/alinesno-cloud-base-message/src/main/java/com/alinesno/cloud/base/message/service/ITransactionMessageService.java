package com.alinesno.cloud.base.message.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.message.entity.TransactionMessageEntity;
import com.alinesno.cloud.base.message.entity.TransactionMessageHistoryEntity;
import com.alinesno.cloud.base.message.repository.TransactionMessageRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 15:19:37
 */
@NoRepositoryBean
public interface ITransactionMessageService
		extends IBaseService<TransactionMessageRepository, TransactionMessageEntity, String> {
	/**
	 * 保存预发送消息
	 * 
	 * @param e
	 * @return 保存的消息id
	 */
	public String saveMessageLocal(TransactionMessageEntity e);

	/**
	 * 确认并发送消息
	 * 
	 * @param id
	 * @return 成功true | 失败false
	 */
	public boolean confirmAndSendMessage(String id);

	/**
	 * 保存消息并直接发送
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	public boolean saveAndSendMessage(TransactionMessageEntity e);

	/**
	 * 直接发送消息，不保存本地
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	public boolean directSendMessage(TransactionMessageEntity e);

	/**
	 * 根据消息id重新发送消息,重新发送的消息,会将消息重发次数加1
	 * 
	 * @param e
	 * @return 成功true | 失败false @
	 */
	public boolean reSendMessageById(String id);

	/**
	 * 根据id将消息标记为死亡状态
	 * 
	 * @param id
	 * @return @
	 */
	public boolean setMessageToAreadlyDead(String id);

	/**
	 * 根据消息id获取消息,其中包括已经死亡的消息，不包括历史消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */

	public TransactionMessageEntity getMessageById(String id);

	/**
	 * 根据消息id获取死亡的消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	public TransactionMessageEntity getDeathMessageById(String id);

	/**
	 * 根据消息id获取历史消息
	 * 
	 * @param id 消息的id
	 * @return 存在返回实体|不存在则返回null @
	 */
	public TransactionMessageHistoryEntity getHistoryMessageById(String id);

	/**
	 * 根据队名获取消息
	 * 
	 * @param String queueName 队列的id
	 * @return @
	 */
	public Page<TransactionMessageEntity> getMessageByTopic(String topic, int pageNow, int pageSize);

	/**
	 * 根据队列名和是否死亡获取消息
	 * 
	 * @param String queueName 队列的id , Map<String, Object> param
	 * @return @
	 */
	public Page<TransactionMessageEntity> getMessageByTopicAndParam(String topic, Map<String, Object> params,
			int pageNow, int pageSize);

	/**
	 * 根据消息的id删除消息,即移到历史表
	 * 
	 * @param id
	 * @return 成功true | 失败false @
	 */
	public boolean deleteMessageById(String id);

	/**
	 * 根据业务id确认消息已经成功消费,然后删除消息id
	 * 
	 * @param id
	 * @return @
	 */
	public boolean confirmMessageByMessageId(String id);

	/**
	 * 重发某个消息列队中的全部死亡消息
	 * 
	 * @param queueName 列队的名称
	 * @param batchSize 发送的条数
	 * @return 成功null | 失败返回失败消息的id @
	 */
	public List<String> reSendAllMessageByTopic(String topic, int batchSize);

	/**
	 * 分页获取消息数据
	 * 
	 * @param          <PageBean>
	 * @param params   参数
	 * @param pageNow  当前页
	 * @param pageSize 分页条数
	 * @return
	 */
	public Page<TransactionMessageEntity> listMessagePage(int pageNow, int pageSize, Map<String, Object> params);

	/**
	 * 发送kafka消息
	 * 
	 * @param messageBody
	 * @param topicName
	 * @return 成功则返回true，否则失败
	 */
	boolean sendMessage(String messageBody, String topic);

}
