package com.alinesno.cloud.operation.cmdb.web.bean;

/**
 * 当前用户信息
 * @author LuoAnDong
 * @since 2018年8月8日 上午8:41:12
 */
public class CurrentUserBean {

	private String id ; // 用户ID
	private String name; // 用户名称
	private String phone; // 用户手机号
	private String masterCode ;// 所属机房
	private String openId; // 用户openID
	private String fieldProp ; 
	
	public CurrentUserBean() {
		super();
	}
	public CurrentUserBean(String id, String name, String phone, String masterCode , String openId) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.masterCode = masterCode ;
		this.openId = openId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFieldProp() {
		return fieldProp;
	}
	public void setFieldProp(String fieldProp) {
		this.fieldProp = fieldProp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getMasterCode() {
		return masterCode;
	}
	public void setMasterCode(String masterCode) {
		this.masterCode = masterCode;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}

}
