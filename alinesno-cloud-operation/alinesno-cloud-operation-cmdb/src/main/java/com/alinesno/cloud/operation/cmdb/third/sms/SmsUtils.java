package com.alinesno.cloud.operation.cmdb.third.sms;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * 短信工具类
 * @author LuoAnDong
 * @since 2018年3月3日 上午9:58:35
 */
public class SmsUtils {

	/**
	 * 生成短信验证码
	 * @param length
	 * @return
	 */
	public static String generateVerificationCode(int length) {
		return RandomStringUtils.random(length, "0123456789");
	}

}