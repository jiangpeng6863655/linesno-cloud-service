package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 商品列表
 * @author LuoAnDong
 * @since 2018年9月9日 上午11:08:18
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_goods_order")
public class GoodsOrderEntity extends BaseEntity {

	private String goodsName ; //商品名称
	private String goodsPrices ; //商品价格
	private int goodsNum ;  //商品数量 
	private String goodsDesc ; //端口描述 
	private String goodsStatus ; //商品状态(0正常|1下架)
	private String goodsPic ; //商品图片
	private int discount ; // 打折(按百分比)
	
	// ------------------------ 订单详情
	private String goodsOrderId ; //订单号唯一值 
	private int selectGoodsNum ; //选择商品数量
	
	public int getSelectGoodsNum() {
		return selectGoodsNum;
	}
	public void setSelectGoodsNum(int selectGoodsNum) {
		this.selectGoodsNum = selectGoodsNum;
	}
	public String getGoodsOrderId() {
		return goodsOrderId;
	}
	public void setGoodsOrderId(String goodsOrderId) {
		this.goodsOrderId = goodsOrderId;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsPrices() {
		return goodsPrices;
	}
	public void setGoodsPrices(String goodsPrices) {
		this.goodsPrices = goodsPrices;
	}
	public int getGoodsNum() {
		return goodsNum;
	}
	public void setGoodsNum(int goodsNum) {
		this.goodsNum = goodsNum;
	}
	public String getGoodsDesc() {
		return goodsDesc;
	}
	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}
	public String getGoodsStatus() {
		return goodsStatus;
	}
	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	public String getGoodsPic() {
		return goodsPic;
	}
	public void setGoodsPic(String goodsPic) {
		this.goodsPic = goodsPic;
	}

}
