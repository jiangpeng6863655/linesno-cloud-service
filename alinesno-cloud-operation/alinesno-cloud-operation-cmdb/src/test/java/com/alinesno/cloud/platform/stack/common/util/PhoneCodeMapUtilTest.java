package com.alinesno.cloud.platform.stack.common.util;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.common.util.CodeBean;
import com.alinesno.cloud.operation.cmdb.common.util.PhoneCodeMapUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PhoneCodeMapUtilTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	String phone = "15578942583" ; 

	@Test
	public void testPut() throws InterruptedException {

		PhoneCodeMapUtil.OUTTIME_SECOND = 2 ; 
		
		CodeBean bean = new CodeBean("1234" , System.currentTimeMillis()) ; 
		PhoneCodeMapUtil.put(phone , bean);
		
		Thread.sleep(1000);
	
		String code = PhoneCodeMapUtil.get(phone) ; 
		logger.info("code = {}" , code);
		
	}

	@Test
	public void testGet() throws InterruptedException {
		long start = System.currentTimeMillis() ; 
		Thread.sleep(1000);
		long end = System.currentTimeMillis() ; 
		
		logger.info("idea time = {}" , (end-start)/1000);
	}

	@Test
	public void testValidateCodeByPhone() {
		fail("Not yet implemented");
	}

}
