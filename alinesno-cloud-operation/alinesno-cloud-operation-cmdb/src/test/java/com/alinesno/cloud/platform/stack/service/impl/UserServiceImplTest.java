package com.alinesno.cloud.platform.stack.service.impl;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.operation.cmdb.entity.UserEntity;
import com.alinesno.cloud.operation.cmdb.service.UserService;
import com.alinesno.cloud.platform.stack.JUnitBase;

public class UserServiceImplTest extends JUnitBase {

	@Autowired
	private UserService userService ; 
	
	@Test
	public void testFindAllMember() {
		String masterCode = "10608" ; 
		String memberId = "" ; // "492373332545503232" ; 
		
		List<UserEntity> users = userService.findAllMember(masterCode, memberId) ; 
		logger.debug("users = {}" , users.size());
	}

}
