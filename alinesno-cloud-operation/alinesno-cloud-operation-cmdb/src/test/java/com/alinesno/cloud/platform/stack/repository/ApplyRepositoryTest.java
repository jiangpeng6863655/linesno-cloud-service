package com.alinesno.cloud.platform.stack.repository;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.ApplyEntity;
import com.alinesno.cloud.operation.cmdb.repository.ApplyRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplyRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private ApplyRepository applyRepository; // 申请服务
	
	String userId = "4782735210095575043" ; 

	@Test
	public void testFindByUserId() {
		ApplyEntity e = applyRepository.findByUserId(userId) ; 
		logger.debug("e = {}" , e);
	}
	
	/**
	 * 测试保存申请
	 */
	@Test
	public void testSave() {
		ApplyEntity apply = new ApplyEntity() ; 
		
		apply.setUserId(userId);
		apply.setAddTime(new Timestamp(System.currentTimeMillis()));
//		apply.setApplyStatus(ApplyStatusEnum.DOING.getCode()); //审批中
		
		apply = applyRepository.save(apply) ; 
		logger.debug("apply = {}" , ToStringBuilder.reflectionToString(apply));
	}

}
