package com.alinesno.cloud.platform.stack.repository;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.SmsTemplateEntity;
import com.alinesno.cloud.operation.cmdb.repository.SmsTemplateRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmsTemplateRepositoryTest {

	@Autowired
	private SmsTemplateRepository smsTemplateRepository ; 
	
	@Test
	public void testSaveAll() {
		
		List<SmsTemplateEntity> list = new ArrayList<SmsTemplateEntity>() ; 
		
		SmsTemplateEntity t1 = new SmsTemplateEntity("您的动态码为：${code}，您正在进行密码重置操作，如非本人操作，请忽略本短信！" , "SMS_126575232" , "密码重置操作") ; 
		SmsTemplateEntity t2 = new SmsTemplateEntity("您有新的订单待处理，当前状态：${status}，订单摘要:${remark}，请及时处理" , "SMS_126580268" , "订单处理通知") ; 
		SmsTemplateEntity t3 = new SmsTemplateEntity("您的验证码：${code}，您正进行身份验证，打死不告诉别人！" , "SMS_126620228" , "用户身份验证") ; 
		SmsTemplateEntity t4 = new SmsTemplateEntity("您正在申请手机注册，验证码为：${code}，5分钟内有效！！" , "SMS_126630218" , "申请手机注册") ; 
	
		list.add(t1) ; 
		list.add(t2) ; 
		list.add(t3) ; 
		list.add(t4) ; 
		
		smsTemplateRepository.saveAll(list) ; 
	}

}
