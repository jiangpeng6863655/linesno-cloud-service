package com.alinesno.cloud.base.boot.web.module.monitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@RequestMapping("boot/platform/monitor")
@Scope(SpringInstanceScope.PROTOTYPE)
public class MonitorController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(MonitorController.class) ; 
	
	/**
	 * 平台监控展示
	 */
	@RequestMapping("display")
    public void display(){
		log.debug("display");
    }
	
	/**
	 * 平台预警管理 
	 */
	@RequestMapping("warning")
    public void warning(){
		log.debug("warning");
    }

	/**
	 * 平台监控管理 
	 */
	@RequestMapping("manager")
    public void manager(){
		log.debug("manager");
    }
	
}
