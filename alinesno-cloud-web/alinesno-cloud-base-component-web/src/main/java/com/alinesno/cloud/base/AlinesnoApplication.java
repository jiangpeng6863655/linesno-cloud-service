package com.alinesno.cloud.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;

import com.alinesno.cloud.base.boot.feign.enable.EnableAlinesnoBaseBoot;
import com.alinesno.cloud.common.web.enable.EnableAlinesnoCommonLogin;

/**
 * 启动入口
 * @author LuoAnDong
 * @EnableEurekaClient // 开启eureka
 * @since 2018年8月7日 上午8:45:02
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class) // 去掉datasources配置
@EnableAsync // 开启异步任务
@EnableEurekaClient  // 开启eureka

@EnableAlinesnoBaseBoot
@EnableAlinesnoCommonLogin
public class AlinesnoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
