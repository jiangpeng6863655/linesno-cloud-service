package com.alinesno.cloud.base.print.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.print.enums.EditStatusEnums;
import com.alinesno.cloud.base.print.feign.dto.TemplateDto ;
import com.alinesno.cloud.base.print.feign.facade.TemplateFeigin ;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;
import com.alinesno.cloud.common.web.base.response.ResponseBean;

/**
 * <p> 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-07 21:24:32
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("base/print/template")
public class TemplateController extends FeignMethodController<TemplateDto, TemplateFeigin> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(TemplateController.class);

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }

	@GetMapping("/all")
    public void list(Model model){
		List<String> title = new ArrayList<String>() ; 
	
		for(int i = 0 ; i < 12 ; i ++) {
			title.add("i"+i) ; 
		}
		
		model.addAttribute("beans", title) ; 
    }
	
	@Override
	public ResponseBean save(Model model, HttpServletRequest request, TemplateDto dto) {
		
		dto.setUseCount(0);
		dto.setEditStatus(EditStatusEnums.NORMAL.getStatus());
		
		return super.save(model, request, dto);
	}


}


























