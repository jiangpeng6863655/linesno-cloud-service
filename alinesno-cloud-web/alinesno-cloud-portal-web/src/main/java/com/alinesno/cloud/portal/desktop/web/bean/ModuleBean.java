package com.alinesno.cloud.portal.desktop.web.bean;

import java.io.Serializable;
import java.util.List;

import com.alinesno.cloud.portal.desktop.web.entity.ModuleEntity;


/**
 * <p>
 * 内容模块
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-20 12:44:59
 */
@SuppressWarnings("serial")
public class ModuleBean implements Serializable {

    /**
     * 模块名称
     */
	private String moduleName;
    /**
     * 模块链接
     */
	private String modulePath;
    /**
     * 模块图标
     */
	private String moduleLogo;
    /**
     * 模块描述
     */
	private String moduleDesc;
    /**
     * 模块排序
     */
	private String moduleSort;
    /**
     * 所属菜单
     */
	private String menusId;
    /**
     * 模块父类
     */
	private String moduleParentId = "0";

	 /**
     * 链接打开状态
     */
	private String openTarget ;
	
	private List<ModuleEntity> subModules ; 
	
	public List<ModuleEntity> getSubModules() {
		return subModules;
	}

	public void setSubModules(List<ModuleEntity> subModules) {
		this.subModules = subModules;
	}

	public String getOpenTarget() {
		return openTarget;
	}

	public void setOpenTarget(String openTarget) {
		this.openTarget = openTarget;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModulePath() {
		return modulePath;
	}

	public void setModulePath(String modulePath) {
		this.modulePath = modulePath;
	}

	public String getModuleLogo() {
		return moduleLogo;
	}

	public void setModuleLogo(String moduleLogo) {
		this.moduleLogo = moduleLogo;
	}

	public String getModuleDesc() {
		return moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public String getModuleSort() {
		return moduleSort;
	}

	public void setModuleSort(String moduleSort) {
		this.moduleSort = moduleSort;
	}

	public String getMenusId() {
		return menusId;
	}

	public void setMenusId(String menusId) {
		this.menusId = menusId;
	}

	public String getModuleParentId() {
		return moduleParentId;
	}

	public void setModuleParentId(String moduleParentId) {
		this.moduleParentId = moduleParentId;
	}


	@Override
	public String toString() {
		return "ModuleEntity{" +
			"moduleName=" + moduleName +
			", modulePath=" + modulePath +
			", moduleLogo=" + moduleLogo +
			", moduleDesc=" + moduleDesc +
			", moduleSort=" + moduleSort +
			", menusId=" + menusId +
			", moduleParentId=" + moduleParentId +
			"}";
	}
}
