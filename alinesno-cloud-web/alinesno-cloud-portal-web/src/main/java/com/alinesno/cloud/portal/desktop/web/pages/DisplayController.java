package com.alinesno.cloud.portal.desktop.web.pages;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 显示设置
 * @author LuoAnDong
 * @since 2019年4月19日 上午6:18:05
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
public class DisplayController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(DisplayController.class) ; 

	@Value("${server.servlet.context-path:/}")
	private String serlvetContextPath ; 
	
	@RequestMapping(value = "display")
	public void display(Model model , HttpServletRequest request) {
	}

	/**
	 * 跳转至其它网站
	 * @param model
	 * @param target
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "public/redirect/site")
	public String targetSite(Model model , String target , HttpServletRequest request) {
		log.debug("target url:{} , serlvetContextPath:{}" , target , serlvetContextPath);
		
		if(StringUtils.isNotBlank(target) && !target.startsWith("http")) {
			target = serlvetContextPath + target ;
		}
		
		model.addAttribute("targetUrl", target) ;
		return "portal/common/site" ; 
	}

	/**
	 * 跳转至gitbook文档 
	 * @param model
	 * @param target
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "public/gitbook/document")
	public String gitbookDocument(Model model , String target , HttpServletRequest request) {
		log.debug("target url:{} , serlvetContextPath:{}" , target , serlvetContextPath);
		
		if(StringUtils.isNotBlank(target) && !target.startsWith("http")) {
			target = serlvetContextPath + target ;
		}
		
		model.addAttribute("targetUrl", target) ;
		return "portal/common/document" ; 
	}
	
}
