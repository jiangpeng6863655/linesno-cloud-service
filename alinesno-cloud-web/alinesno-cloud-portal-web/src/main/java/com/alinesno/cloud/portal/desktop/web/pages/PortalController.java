package com.alinesno.cloud.portal.desktop.web.pages;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;
import com.alinesno.cloud.portal.desktop.web.bean.MenusBean;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathEntity;
import com.alinesno.cloud.portal.desktop.web.entity.UserFunctionEntity;
import com.alinesno.cloud.portal.desktop.web.entity.UserModuleEntity;
import com.alinesno.cloud.portal.desktop.web.repository.LinkPathRepository;
import com.alinesno.cloud.portal.desktop.web.service.IMenusService;
import com.alinesno.cloud.portal.desktop.web.service.IModuleService;
import com.alinesno.cloud.portal.desktop.web.service.IUserFunctionService;
import com.alinesno.cloud.portal.desktop.web.service.IUserModuleService;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
public class PortalController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(PortalController.class) ; 

	@Autowired
	private IMenusService menusService ; 
	
	@Autowired
	private IUserFunctionService userFunctionService ; 
	
	@Autowired
	private IUserModuleService userModuleService ; 
	
	@Autowired
	private LinkPathRepository linkPathRepository ; 
	
	@SuppressWarnings("unused")
	@Autowired
	private IModuleService moduleService ; 
	
	@RequestMapping(value = "/public/portal")
	public String index(Model model , HttpServletRequest request) {
		log.debug("进入首页");
		
		ManagerAccountDto account = CurrentAccountSession.get(request) ; 
		if(account != null) {
			// 最近使用
			List<UserModuleEntity> os = userModuleService.findOftenUse(account.getId()) ; 
			request.setAttribute("os", os); 
			
			// 我的模块
			List<UserModuleEntity> ms = userModuleService.findAllByUid(account.getId()) ; 
			request.setAttribute("ms", ms); 
			
			// 我的常用功能
			List<UserFunctionEntity> fs = userFunctionService.findAllByUid(account.getId()) ; 
			request.setAttribute("fs", fs); 
			
			return "portal/views/ucenter" ; 
		}
	
		return "portal/views/index" ; 
	}

	/**
	 * 显示所有菜单 
	 * @param model
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/public/navigation")
	public ResponseBean navigation(Model model , HttpServletRequest request) {
		log.debug("进入首页");
	
		List<MenusBean> menus = menusService.findNavigation() ; 
		model.addAttribute("menus", menus) ; 
		
		for(MenusBean b : menus) {
			log.debug("menusBean:{}" , ToStringBuilder.reflectionToString(b));
		}
		
		return ResponseGenerator.genSuccessResult(menus) ; 
	}

	/**
	 * 显示所有链接
	 * @param model
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/public/links")
	public ResponseBean links(Model model , String type , HttpServletRequest request) {
		log.debug("链接查询,类型:{}" , type);
	
		List<LinkPathEntity> menus = linkPathRepository.findAllByLinkDesign(type) ; 
		model.addAttribute("menus", menus) ; 
		
		for(LinkPathEntity b : menus) {
			log.debug("LinkPathEntity:{}" , ToStringBuilder.reflectionToString(b));
		}
		
		return ResponseGenerator.genSuccessResult(menus) ; 
	}
}
